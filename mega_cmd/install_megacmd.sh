#!/bin/bash

#installation rapide : 
# go to https://mega.nz/cmd
#for debian jessie
sudo apt install autoconf build-essential libtool g++ libcrypto++-dev libz-dev libsqlite3-dev libssl-dev libcurl4-gnutls-dev libreadline-dev libpcre++-dev libsodium-dev libc-ares-dev libfreeimage-dev libavcodec-dev libavutil-dev libavformat-dev libswscale-dev libmediainfo-dev libzen-dev git -y
cd
sudo dpkg --configure -a
versionNumber=$(cat /etc/os-release | grep VERSION_ID | cut -d'=' -f2 | tr -d "\"")
os=$(cat /etc/os-release | grep -E ^ID | cut -d'=' -f2)
arch=$(sudo dpkg --print-architecture)
if [ "$os" == "debian" ]; then
  if [ "$versionNumber" -eq "8" ]; then
	if [ "$arch" == "i386" ]; then
  		echo "version = 8 i386"
	elif [ "$arch" == "amd64" ]; then
		echo "version = 8 amd64"
		wget https://mega.nz/linux/MEGAsync/Debian_8.0/amd64/megacmd-Debian_8.0_amd64.deb
		sudo dpkg -i megacmd-Debian_8.0_amd64.deb
	else
		echo "architecture not supported"
	fi
  elif [ "$versionNumber" -eq "9" ]; then
	
        if [ "$arch" == "i386" ]; then
        	echo "version 9 i386"
		wget https://mega.nz/linux/MEGAsync/Debian_9.0/i386/megacmd_1.1.0+4.1_i386.deb
		sudo dpkg -i megacmd_1.1.0+4.1_i386.deb
	elif [ "$arch" == "amd64" ]; then
		echo "version = 9 amd64"
		wget https://mega.nz/linux/MEGAsync/Debian_9.0/amd64/megacmd_1.1.0+4.1_amd64.deb
		sudo dpkg -i megacmd_1.1.0+4.1_amd64.deb
	else
		echo "architecture not supported"
	fi
  elif [ "$versionNumber" -eq "10" ]; then
	 if [ "$arch" == "i386" ]; then
        	echo "version 10 i386"
		wget https://mega.nz/linux/MEGAsync/Debian_9.0/i386/megacmd_1.1.0+4.1_i386.deb
		sudo dpkg -i megacmd_1.1.0+4.1_i386.deb
	elif [ "$arch" == "amd64" ]; then
		echo "version = 10 amd64"
		wget https://mega.nz/linux/MEGAsync/Debian_10.0/amd64/megacmd_1.1.0+12.1_amd64.deb
		sudo dpkg -i megacmd_1.1.0+12.1_amd64.deb
	else
		echo "architecture not supported"
	fi	
  else
	echo "version not supported"
  fi
else
  echo "os not supported"
fi






#wget https://mega.nz/linux/MEGAsync/Debian_9.0/amd64/megacmd_1.1.0+4.1_amd64.deb
#sudo dpkg -i megacmd_1.1.0+4.1_amd64.deb

#sur debian jessie, nous avons une erreur à la commmande make qui indique un souci avec la version de openssl => donc le mieux est d'installer mega rapidement en
#suivant le protocole ci-dessus
#sudo apt install autoconf build-essential libtool g++ libcrypto++-dev libz-dev libsqlite3-dev libssl-dev libcurl4-gnutls-dev libreadline-dev libpcre++-dev libsodium-dev libc-ares-dev libfreeimage-dev libavcodec-dev libavutil-dev libavformat-dev libswscale-dev libmediainfo-dev libzen-dev git -y
#sudo apt-get install libcurl4-openssl-dev -y
#git clone https://github.com/meganz/MEGAcmd.git
#cd MEGAcmd && git submodule update --init --recursive
#sh autogen.sh
#./configure
#make
#sudo make install
#sudo ldconfig


#on ssh sdg (openvz)
#https://megatools.megous.com/
#install megatool
#cd ~
#sudo apt-get -y -q install build-essential pkg-config libglib2.0-dev libssl-dev libcurl4-openssl-dev libfuse-dev glib-networking gcc asciidoc vim git make
#wget https://megatools.megous.com/builds/megatools-1.10.2.tar.gz \-qO m.tar.gz && tar -xzf m.tar.gz && cd megatools* && CFLAGS="-std=gnu99" ./configure --prefix=/etc/megatools && make -j4 && sudo make install && sudo ldconfig
#cd ~
#touch ~/megatools-1.10.2/.megarc
#chmod a+x .megarc
#megatools-1.10.2/megacopy --config=~/megatools-1.10.2/.megarc -r /Root/install_gmixer.sh -l ./ -d




