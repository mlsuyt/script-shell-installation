#!/usr/bin/env sh

#set -xe
VERSION="1.0"

# VARIABLES
blank="\033[1;37m"
grey="\033[0;37m"
pink="\033[0;35m"
red="\033[1;31m"
green="\033[1;32m"
yellow="\033[1;33m"
blue="\033[1;34m"
rescolor="\033[0m"

#ok sur debian 9 amd64 => juste invalid token à la fin, mais l'url est accessible
#ok sur debian 8 amd64 => juste invalid token à la fin, mais l'url est accessible

#todo
#il faut faire sudo dpkg --configure -a et rendre cela automatique sinon upgrade sur debian 10 amd64 ne fonctionne pas
#sudo dpkg --configure -a --force-confold

#check what is installed before executing the script
##    nginx
#    PostgreSQL >= 9.6
#    Redis >= 2.8.18
#    NodeJS >= 10.x
#    yarn >= 1.x
#    FFmpeg >= 3.x

# Start of script
printf "\n%b$green\n"
printf "####################################################################################################\n"
printf "#                                                                                                  #\n"
printf "#                                  Installation of Peertube                                        #\n"
printf "#                                                                       			   #\n"
printf "#                            Tested on Debian 9 x64,8 x64 and 10 x64                               #\n"
printf "#                                             by @pytlin                                  	   #\n"
printf "#                                                                       			   #\n"
printf "#                        For any bug please send mail to pytlin@protonmail.com             	   #\n"
printf "#                                                  OR                                     	   #\n"
printf "# Create issue here : https://gitlab.com/mlsuyt/script-shell-installation/tree/master/peertube     #\n"
printf "#                                                                       			   #\n"
printf "#                                                                       			   #\n"
printf "#              For the moment only Debian 8, 9 and 10 operating systems are supported		   #\n"
printf "#                                                                       			   #\n"
printf "####################################################################################################\n"
sleep 3


if [ "$1" = "" ];then
        echo "Path of .env not found, please enter a path for .env as argument"
	exit 1
else
	pathEnv="$1"
fi 

#------------------------------------------------ Functions -----------------------------------------------------------
printOkKo() {
	
	if test "$noError" = "true"
	then
		printf "%bOK%b\n" "${green}" "${rescolor}"
	else 
		printf "%bKO%b\n" "${red}" "${rescolor}"
		printf "%bThe installation is aborted due to error%b\n" "${red}" "${rescolor}"
		exit 1
	fi

}

checkCommand() {

	if [ $? -eq 0 ]; then
		noError="true"
	else 
		noError="false"
	fi

}

checkOs1() {

versionNumber=$(cat /etc/os-release | grep VERSION_ID | cut -d'=' -f2 | tr -d "\"")
os=$(cat /etc/os-release | grep -E ^ID= | cut -d'=' -f2)
arch=$(sudo dpkg --print-architecture)
if [ "$os" = "debian" ]; then
	printf "%bOK\n%bYour OS : %s is supported.%b" "${green}" "${green}" "${os}" "${rescolor}"
else
	printf "\n%bYour OS : %s is not yet supported, end of script.%b" "${red}" "${os}" "${rescolor}"
	exit 1
fi
}

checkServicesStarted() {

nginxStarted=$(sudo systemctl status nginx | grep "(running)" | wc -l)
psqlStarted=$(sudo systemctl status postgresql | grep "active (exited)" | wc -l)
redisStarted=$(sudo systemctl status redis_1234 | grep "active (running)" | wc -l)
peertubeStarted=$(sudo systemctl status peertube | grep "active (running)" | wc -l)
peertubeStarted1=$(sudo cat /var/log/syslog | grep "peertube" | grep "Web server: http" | wc -l)

if [ "$nginxStarted" -eq "1" ]; then
  if [ "$psqlStarted" -eq "1" ]; then
    if [ "$redisStarted" -eq "1" ]; then
      if [ "$peertubeStarted" -eq "1" ]; then
        if [ "$peertubeStarted1" -eq "1" ]; then
          printf "\n%bAll services for peertube (nginx, postgresql, redis and peertube) are started...%b\n" "${green}" "${rescolor}"
	fi
      else
	printf "\n%bService Peertube is stopped or has an issue, please start the service or check the log...%b" "${red}" "${rescolor}"
      fi
   else
     printf "\n%bService redis is stopped or has an issue, please start the service or check the log...%b" "${red}" "${rescolor}"
   fi
  else
     printf "\n%bService postgresql is stopped or has an issue, please start the service or check the log...%b" "${red}" "${rescolor}"
  fi
else
     printf "\n%bService nginx is stopped or has an issue, please start the service or check the log...%b" "${red}" "${rescolor}"
fi

}
#------------------------------------------------ Functions ------------------------------------------------------------------------



logFile=$(mktemp /tmp/log.XXX)
ip=$(hostname -I | cut -d' ' -f1)
domain=$(hostname -I | cut -d' ' -f1 | tr -d ' ')
mailAdminPeertube=$(grep mailAdminPeertube $pathEnv | cut -d '=' -f2) 
versionNode=$(grep versionNode $pathEnv | cut -d '=' -f2 | tr -d '"')
timezone=$(grep timezone $pathEnv | cut -d '=' -f2 | tr -d '"')
userSystemPeertube=$(grep userSystemPeertube $pathEnv | cut -d '=' -f2 | tr -d '"')
pwdSystemPeertube=$(grep pwdSystemPeertube $pathEnv | cut -d '=' -f2)
userPostgres=$(grep userPostgres $pathEnv | cut -d '=' -f2 | tr -d '"')
pwdPostgres=$(grep pwdPostgres $pathEnv | cut -d '=' -f2 | tr -d '"')
dbPeertube=$(grep dbPeertube $pathEnv | cut -d '=' -f2 | tr -d '"')
path_website=$(grep path_website $pathEnv | cut -d '=' -f2 | tr -d '"')
folder_website=$(grep folder_website $pathEnv | cut -d '=' -f2 | tr -d '"')
port_redis=$(grep port_redis $pathEnv | cut -d '=' -f2)
userSystem=$(grep user_System $pathEnv | cut -d '=' -f2 | tr -d '"')
userNginx=$(grep userNginx $pathEnv | cut -d '=' -f2 | tr -d '"')
portPeertube=$(grep portPeertube $pathEnv | cut -d '=' -f2)
path_nginx=$(grep path_nginx $pathEnv | cut -d '=' -f2 | tr -d '"')
suffix=$(grep suffix $pathEnv | cut -d '=' -f2 | tr -d '"')




sudo dpkg --configure -a --force-confold
printf "\n%bThe log file is : %s %b" "${red}" "${logFile}" "${rescolor}"
printf "\n%bCheck if your Os is supported...%b" "${yellow}" "${rescolor}"
checkOs1
checkCommand
printOkKo
printf "%bUpdate system...%b" "${yellow}" "${rescolor}"
sudo apt-get update -y >> $logFile && sudo apt-get upgrade -y >> $logFile 2>&1
checkCommand
printOkKo
printf "%bInstallation of some packages...%b" "${yellow}" "${rescolor}"

#on debian 10 amd64 tcl8.5 not exist but tcl8.6 exist
paquet=$(sudo apt-cache search tcl8.5 | wc -l)
if [ "$paquet" -eq "0" ]; then
	paquet="tcl8.6"
else
	paquet="tcl8.5"
fi

if [ "$os" = "debian" ]; then
	if [ "$versionNumber" = "8" ]; then
		sudo apt-get install unzip pgadmin3 pgtop pg-activity postgresql postgresql-contrib g++ make redis-tools redis-server git xauth mediainfo vim apt-transport-https dirmngr ufw htop iperf speedtest-cli curl fail2ban nginx redis-server build-essential gcc $paquet -y >> $logFile 2>&1
	else
		sudo apt-get install unzip pgadmin3 pgtop pg-activity postgresql postgresql-contrib g++ make redis-tools redis-server git xauth mediainfo certbot python-certbot-nginx vim apt-transport-https dirmngr ufw htop iperf speedtest-cli curl fail2ban nginx redis-server build-essential gcc $paquet ffmpeg -y >> $logFile 2>&1
	fi
fi
checkCommand
printOkKo
redisInstalled=$($(which redis-server) -v | grep "5.0.7" | wc -l)
if [ "$redisInstalled" -ne "1" ]; then
if [ ! -e $HOME/redis-stable.tar.gz ]; then
printf "%bDownload of Redis...%b" "${yellow}" "${rescolor}"
wget -q http://download.redis.io/releases/redis-stable.tar.gz -O $HOME/redis-stable.tar.gz >> $logFile 2>&1
checkCommand
printOkKo
fi
if [ ! -d $HOME/redis-stable ]; then
printf "%bUntar Redis archive in $HOME...%b" "${yellow}" "${rescolor}"
tar xzf $HOME/redis-stable.tar.gz -C $HOME >> $logFile 2>&1
checkCommand
printOkKo
fi
cd $HOME/redis-stable
printf "%bBuild Redis...%b" "${yellow}" "${rescolor}"
export MALLOC=libc; make distclean >> $logFile 2>&1 && make >> $logFile 2>&1
checkCommand
printOkKo
# optionally run "make test" to check everything is ok
printf "%bInstallation of Redis...%b" "${yellow}" "${rescolor}"
sudo make install >> $logFile 2>&1
checkCommand
printOkKo
fi
redisStarted=$(sudo systemctl status redis_${port_redis} | grep "(running)" | wc -l)
if [ "$redisStarted" -ne "1" ]; then
sudo REDIS_PORT=${port_redis} REDIS_CONFIG_FILE=/etc/redis/${port_redis}.conf REDIS_LOG_FILE=/var/log/redis_$port_redis.log REDIS_DATA_DIR=/var/lib/redis/$port_redis REDIS_EXECUTABLE=$(command -v ~/redis-stable/src/redis-server) ./utils/install_server.sh >> $logFile 2>&1
printf "%bStart Redis service...%b" "${yellow}" "${rescolor}"
sudo service redis_${port_redis} start >> $logFile 2>&1
checkCommand
printOkKo
fi
printf "%bCreation of web folder if not exists...%b" "${yellow}" "${rescolor}"
if [ ! -d $path_website ]; then
	sudo mkdir -p $path_website >> $logFile 2>&1
fi
checkCommand
printOkKo
printf "%bCreation of user Peertube if not exists..%b" "${yellow}" "${rescolor}"
userId=$(id -u $userSystemPeertube) >> $logFile 2>&1
test1=$(echo $userId | wc -c)
if [ "$test1" -eq "1" ]; then
	sudo /usr/sbin/useradd -m -d $path_website$folder_website -s /bin/bash -p $pwdSystemPeertube $userSystemPeertube >> $logFile 2>&1
	sudo /usr/sbin/usermod -aG sudo $userSystemPeertube >> $logFile 2>&1
	echo "$userSystemPeertube:$pwdSystemPeertube" | sudo /usr/sbin/chpasswd >> $logFile 2>&1
fi
checkCommand
printOkKo
printf "%bSet the right timezone...%b" "${yellow}" "${rescolor}"
sudo timedatectl set-timezone $timezone >> $logFile 2>&1
checkCommand
printOkKo
rightVersionNode=$(nodejs -v | grep -e "12" -e "11" -e "10" | wc -l)
if [ "$rightVersionNode" -ne "1" ]; then
printf "%bInstallation of nodejs...%b" "${yellow}" "${rescolor}"
curl -sL https://deb.nodesource.com/setup_$versionNode.x | sudo -E bash - >> $logFile 2>&1
if [ "$versionNode" = "12" ]; then
	sudo apt-get install -y nodejs >> $logFile 2>&1
else
	sudo apt-get install -y nodejs npm >> $logFile 2>&1
fi
#curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh -o install_nvm.sh >> $logFile 2>&1
#bash install_nvm.sh >> $logFile 2>&1
#nvm ls-remote
#install latest lts version : 
#nvm install 12.14.0 >> $logFile 2>&1
checkCommand
printOkKo
fi
yarnInstalled=$(yarn -v | cut -d'.' -f1 | grep "1" | wc -l)
if [ "$yarnInstalled" -ne "1" ]; then
printf "%bInstallation of yarn...%b" "${yellow}" "${rescolor}"
sudo apt-get purge cmdtest yarn -y >> $logFile 2>&1
sudo curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - >> $logFile 2>&1
sudo echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list >> $logFile 2>&1
sudo apt-get update -y >> $logFile 2>&1 && sudo apt-get install --no-install-recommends yarn -y >> $logFile 2>&1
yarn --version >> $logFile 2>&1
checkCommand
printOkKo
fi
printf "%bCreation of the role postgresql if not exists...%b" "${yellow}" "${rescolor}"
psqlUserExist=$(sudo -i -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='$userPostgres'")
if [ "$psqlUserExist" != "1" ]; then
	sudo -i -u postgres psql -c "CREATE USER $userPostgres WITH PASSWORD '$pwdPostgres';" >> $logFile 2>&1
fi
checkCommand
printOkKo
printf "%bCreation of postgresql database and activation of extensions required for Peertube...%b" "${yellow}" "${rescolor}"
psqlDbExists=$(sudo -i -u postgres psql -lqt | cut -d \| -f 1 | grep -w $dbPeertube | wc -l)
if [ "$psqlDbExists" -ne "1" ]; then
	sudo -u postgres createdb -O $userPostgres $dbPeertube
	sudo -u postgres psql -c "CREATE EXTENSION pg_trgm;" $dbPeertube >> $logFile 2>&1
	sudo -u postgres psql -c "CREATE EXTENSION unaccent;" $dbPeertube >> $logFile 2>&1
fi
checkCommand
printOkKo
printf "%bDownload of Peertube source...%b" "${yellow}" "${rescolor}"
sudo usermod -aG $userNginx $userSystem
if [ ! -d $path_website$folder_website/config ]; then
	sudo mkdir -p $path_website$folder_website/config 
fi
if [ ! -d $path_website$folder_website/storage ]; then
	sudo mkdir -p $path_website$folder_website/storage 
fi
if [ ! -d $path_website$folder_website/versions ]; then
	sudo mkdir -p $path_website$folder_website/versions
fi
sudo chown -R $userNginx:$userNginx $path_website
sudo chmod -R 777 $path_website$folder_website
sudo chmod -R 777 $path_website$folder_website/versions
cd $path_website$folder_website/versions
export VERSION=$(curl -s https://api.github.com/repos/chocobozzz/peertube/releases/latest | grep tag_name | cut -d '"' -f 4) >> $logFile 2>&1 && echo "Latest Peertube version is $VERSION" >> $logFile 2>&1
sudo wget -q "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip" -O ./peertube-${VERSION}.zip >> $logFile 2>&1
sudo unzip -o ./peertube-${VERSION}.zip >> $logFile 2>&1 && rm -rf ./peertube-${VERSION}.zip >> $logFile 2>&1
checkCommand
printOkKo
printf "%bBuild of Peertube...%b" "${yellow}" "${rescolor}"
#nvm use node
#sudo ln -s $(which npm) /usr/bin/npm
#sudo ln -s $(which node) /usr/bin/nodejs #nodejs car yarn ne reconnaît pas node, mais nodejs)
#sur architecture 32 bits, il faut installer manuellement libvips (>8.8.1) qui a besoin de glib2.0-dev
#sudo apt-get install glib2.0-dev
#wget https://github.com/libvips/libvips/releases/download/v8.8.4/vips-8.8.4.tar.gz
#tar xf vips-8.8.4.tar.gz
#cd vips-8.8.4
#./configure
#make
#sudo make install
#sudo ldconfig
#checker si node v4 est installé et si oui le désintaller
#sudo apt-get remove node
if [ ! -e $path_website$folder_website/peertube-latest ]; then
	sudo ln -s $path_website$folder_website/versions/peertube-${VERSION} $path_website$folder_website/peertube-latest
fi
cd $path_website$folder_website/peertube-latest
sudo yarn install --production --pure-lockfile >> $logFile 2>&1
checkCommand
printOkKo
printf "%bConfiguration of Peertube...%b" "${yellow}" "${rescolor}"
if [ -f "$path_website$folder_website/config/production.yaml" ]; then
	rm -rf "$path_website$folder_website/config/production.yaml" >> $logFile 2>&1
else
	sudo touch "$path_website$folder_website/config/production.yaml" >> $logFile 2>&1
fi
sudo tee -a "$path_website$folder_website/config/production.yaml" >> $logFile 2>&1 << END
listen:
  hostname: 127.0.0.1
  port: 9000
webserver:
  https: false
  hostname: $domain
  port: $portPeertube
rates_limit:
  api:
    window: '10 seconds'
    max: 50
  login:
    window: '5 minutes'
    max: 15
  signup:
    window: '5 minutes'
    max: 2
  ask_send_email:
    window: '5 minutes'
    max: 3
trust_proxy:
  - loopback
database:
  hostname: 127.0.0.1
  port: 5432
  suffix: '$suffix'
  username: '$userPostgres'
  password: '$pwdPostgres'
  pool:
    max: 5
redis:
  hostname: 127.0.0.1
  port: $port_redis
  auth: null
  db: 0
smtp:
  hostname: null
  port: 465
  username: null
  password: null
  tls: true
  disable_starttls: false
  ca_file: null
  from_address: admin@example.com
email:
  body:
    signature: PeerTube
  subject:
    prefix: '[PeerTube]'
storage:
  tmp: $path_website$folder_website/storage/tmp/
  avatars: $path_website$folder_website/storage/avatars/
  videos: $path_website$folder_website/storage/videos/
  streaming_playlists: $path_website$folder_website/storage/streaming-playlists/
  redundancy: $path_website$folder_website/storage/redundancy/
  logs: $path_website$folder_website/storage/logs/
  previews: $path_website$folder_website/storage/previews/
  thumbnails: $path_website$folder_website/storage/thumbnails/
  torrents: $path_website$folder_website/storage/torrents/
  captions: $path_website$folder_website/storage/captions/
  cache: $path_website$folder_website/storage/cache/
  plugins: $path_website$folder_website/storage/plugins/
log:
  level: info
  rotation:
    enabled: true
search:
  remote_uri:
    users: true
    anonymous: false
trending:
  videos:
    interval_days: 7
redundancy:
  videos:
    check_interval: '1 hour'
    strategies: null
csp:
  enabled: false
  report_only: true
  report_uri: null
tracker:
  enabled: true
  private: true
  reject_too_many_announces: false
history:
  videos:
    max_age: -1
views:
  videos:
    remote:
      max_age: -1
plugins:
  index:
    enabled: true
    check_latest_versions_interval: '12 hours'
    url: 'https://packages.joinpeertube.org'
cache:
  previews:
    size: 500
  captions:
    size: 500
admin:
  email: admin@example.com
contact_form:
  enabled: true
signup:
  enabled: false
  limit: 10
  requires_email_verification: false
  filters:
    cidr:
      whitelist: []
      blacklist: []
user:
  video_quota: -1
  video_quota_daily: -1
transcoding:
  enabled: false
  allow_additional_extensions: true
  allow_audio_files: true
  threads: 1
  resolutions:
    240p: false
    360p: false
    480p: true
    720p: true
    1080p: false
    2160p: false
  hls:
    enabled: true
import:
  videos:
    http:
      enabled: true
    torrent:
      enabled: true
auto_blacklist:
  videos:
    of_users:
      enabled: false
instance:
  name: PeerTube
  short_description: 'PeerTube, a federated (ActivityPub) video streaming platform using P2P (BitTorrent) directly in the web browser with WebTorrent and Angular.'
  description: 'Welcome to this PeerTube instance!'
  terms: 'No terms for now.'
  code_of_conduct: ""
  moderation_information: ""
  creation_reason: ""
  administrator: ""
  maintenance_lifetime: ""
  business_model: ""
  hardware_information: ""
  languages: null
  categories: null
  default_client_route: /videos/trending
  is_nsfw: false
  default_nsfw_policy: do_not_list
  customizations:
    javascript: ""
    css: ""
  robots: "User-agent: *\nDisallow:\n"
  securitytxt: "# If you would like to report a security issue\n# you may report it to:\nContact: https://github.com/Chocobozzz/PeerTube/blob/develop/SECURITY.md\nContact: mailto:"
services:
  twitter:
    username: '@Chocobozzz'
    whitelisted: false
followers:
  instance:
    enabled: true
    manual_approval: false
followings:
  instance:
    auto_follow_back:
      enabled: false
    auto_follow_index:
      enabled: false
      index_url: 'https://instances.joinpeertube.org'
theme:
  default: default

END
checkCommand
printOkKo





printf "%bConfiguration of vhost nginx...%b" "${yellow}" "${rescolor}"
if [ -e "$path_ningx/peertube.conf" ]; then
	rm -rf "$path_nginx/peertube.conf"
else
	sudo touch $path_nginx/peertube.conf
fi
sudo tee -a $path_nginx/peertube.conf >> $logFile 2>&1 << END
server {
  listen $portPeertube;
  server_name $domain;


  # Enable compression for JS/CSS/HTML bundle, for improved client load times.
  # It might be nice to compress JSON, but leaving that out to protect against potential
  # compression+encryption information leak attacks like BREACH.
  gzip on;
  #gzip_types text/css text/html application/javascript;
  gzip_vary on;


  access_log /var/log/nginx/peertube.ptn.services.access.log;
  error_log /var/log/nginx/peertube.ptn.services.error.log;


  # Bypass PeerTube for performance reasons. Could be removed
  location ~ ^/client/(.*\.(js|css|woff2|otf|ttf|woff|eot))$ {
    add_header Cache-Control "public, max-age=31536000, immutable";

    alias $path_website$folder_website/peertube-latest/client/dist/\$1;
  }

  # Bypass PeerTube for performance reasons. Could be removed
  location ~ ^/static/(thumbnails|avatars)/ {
    if (\$request_method = 'OPTIONS') {
      add_header 'Access-Control-Allow-Origin' '*';
      add_header 'Access-Control-Allow-Methods' 'GET, OPTIONS';
      add_header 'Access-Control-Allow-Headers' 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
      add_header 'Access-Control-Max-Age' 1728000;
      add_header 'Content-Type' 'text/plain charset=UTF-8';
      add_header 'Content-Length' 0;
      return 204;
    }

    add_header 'Access-Control-Allow-Origin' '*';
    add_header 'Access-Control-Allow-Methods' 'GET, OPTIONS';
    add_header 'Access-Control-Allow-Headers' 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';

    # Cache 2 hours
    add_header Cache-Control "public, max-age=7200";

    root $path_website$folder_website/storage;

    rewrite ^/static/(thumbnails|avatars)/(.*)$ /\$1/\$2 break;
    try_files \$uri /;
  }

  location / {
    proxy_pass http://127.0.0.1:9000;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;

    # This is the maximum upload size, which roughly matches the maximum size of a video file
    # you can send via the API or the web interface. By default this is 8GB, but administrators
    # can increase or decrease the limit. Currently there's no way to communicate this limit
    # to users automatically, so you may want to leave a note in your instance 'about' page if
    # you change this.
    #
    # Note that temporary space is needed equal to the total size of all concurrent uploads.
    # This data gets stored in /var/lib/nginx by default, so you may want to put this directory
    # on a dedicated filesystem.
    #
    client_max_body_size 8G;

    proxy_connect_timeout       600;
    proxy_send_timeout          600;
    proxy_read_timeout          600;
    send_timeout                600;
  }

  # Bypass PeerTube for performance reasons. Could be removed
  location ~ ^/static/(webseed|redundancy)/ {
    # Clients usually have 4 simultaneous webseed connections, so the real limit is 3MB/s per client
    limit_rate 800k;

    if (\$request_method = 'OPTIONS') {
      add_header 'Access-Control-Allow-Origin' '*';
      add_header 'Access-Control-Allow-Methods' 'GET, OPTIONS';
      add_header 'Access-Control-Allow-Headers' 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
      add_header 'Access-Control-Max-Age' 1728000;
      add_header 'Content-Type' 'text/plain charset=UTF-8';
      add_header 'Content-Length' 0;
      return 204;
    }

    if (\$request_method = 'GET') {
      add_header 'Access-Control-Allow-Origin' '*';
      add_header 'Access-Control-Allow-Methods' 'GET, OPTIONS';
      add_header 'Access-Control-Allow-Headers' 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';

      # Don't spam access log file with byte range requests
      access_log off;
    }

    root $path_website$folder_website/storage;

    rewrite ^/static/webseed/(.*)$ /videos/\$1 break;
    rewrite ^/static/redundancy/(.*)$ /redundancy/\$1 break;

    try_files \$uri /;
  }

  # Websocket tracker
  location /tracker/socket {
    # Peers send a message to the tracker every 15 minutes
    # Don't close the websocket before this time
    proxy_read_timeout 1200s;
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_http_version 1.1;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header Host \$host;
    proxy_pass http://127.0.0.1:9000;
  }

  location /socket.io {
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header Host \$host;

    proxy_pass http://127.0.0.1:9000;

    # enable WebSockets
    proxy_http_version 1.1;
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection "upgrade";
  }
}
END
checkCommand
printOkKo




printf "%bCreation of permission script...%b" "${yellow}" "${rescolor}"
sudo touch /root/permissions.sh
sudo tee -a /root/permissions.sh >> $logFile 2>&1 << END
#!/bin/bash
#sudo find $path_website$folder_website -type f -print0 | xargs -0 sudo chmod 0640
#sudo find $path_website$folder_website -type d -print0 | xargs -0 sudo chmod 0750
sudo find $path_website$folder_website -type f -print0 | xargs -0 sudo chmod 0777
sudo find $path_website$folder_website -type d -print0 | xargs -0 sudo chmod 0777
chown -R www-data:www-data /var/www/
#chmod 600 /etc/letsencrypt/rsa-certs/fullchain.pem
#chmod 600 /etc/letsencrypt/rsa-certs/privkey.pem
#chmod 600 /etc/letsencrypt/rsa-certs/chain.pem
#chmod 600 /etc/letsencrypt/rsa-certs/cert.pem
#chmod 600 /etc/letsencrypt/ecc-certs/fullchain.pem
#chmod 600 /etc/letsencrypt/ecc-certs/privkey.pem
#chmod 600 /etc/letsencrypt/ecc-certs/chain.pem
#chmod 600 /etc/letsencrypt/ecc-certs/cert.pem
#chmod 600 /etc/ssl/certs/dhparam.pem
exit 0
END
checkCommand
printOkKo


printf "%bExecution of permission script...%b" "${yellow}" "${rescolor}"
sudo chmod +x /root/permissions.sh && sudo /root/permissions.sh  >> $logFile 2>&1
checkCommand
printOkKo

printf "%bCreation of service file for peertube...%b" "${yellow}" "${rescolor}"
if [ -f /etc/systemd/system/peertube.service ]; then
	sudo rm -rf /etc/systemd/system/peertube.service
else
	sudo touch /etc/systemd/system/peertube.service
fi
sudo tee -a /etc/systemd/system/peertube.service >> $logFile 2>&1 << END
[Unit]
Description=PeerTube daemon
After=network.target postgresql.service redis-server.service

[Service]
Type=simple
Environment=NODE_ENV=production
Environment=NODE_CONFIG_DIR=$path_website$folder_website/config
User=$userSystemPeertube
Group=$groupSystemPeertube
ExecStart=/usr/bin/npm start
WorkingDirectory=$path_website$folder_website/peertube-latest
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=peertube
Restart=always

; Some security directives.
; Use private /tmp and /var/tmp folders inside a new file system namespace,
; which are discarded after the process stops.
PrivateTmp=true
; Mount /usr, /boot, and /etc as read-only for processes invoked by this service.
ProtectSystem=full
; Sets up a new /dev mount for the process and only adds API pseudo devices
; like /dev/null, /dev/zero or /dev/random but not physical devices. Disabled
; by default because it may not work on devices like the Raspberry Pi.
PrivateDevices=false
; Ensures that the service process and all its children can never gain new
; privileges through execve().
NoNewPrivileges=true
; This makes /home, /root, and /run/user inaccessible and empty for processes invoked
; by this unit. Make sure that you do not depend on data inside these folders.
ProtectHome=true
; Drops the sys admin capability from the daemon.
CapabilityBoundingSet=~CAP_SYS_ADMIN

[Install]
WantedBy=multi-user.target
END
checkCommand
printOkKo


sudo systemctl daemon-reload
sudo systemctl enable peertube nginx redis_{$port_redis} postgresql


printf "%bRestart of services : nginx, postgresql, redis and peertube...%b" "${yellow}" "${rescolor}"
sudo systemctl restart nginx >> $logFile 2>&1
sudo systemctl restart postgresql >> $logFile 2>&1
sudo systemctl restart redis_${port_redis} >> $logFile 2>&1
sudo systemctl restart peertube >> $logFile 2>&1

checkServicesStarted

cd $path_website$folder_website/peertube-latest
sudo -u peertube NODE_CONFIG_DIR=$path_website$folder_website/config NODE_ENV=production npm run reset-password -- -u root



